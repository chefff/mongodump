
About Repo 

This is a dump of a sample DB for atipacs project to be restored by MongoDB . This sample was dumped at 17:06 on 06/Feb/2021.


How to Restore

To restore this DB, git clone and cd into the repo top directory in shell. Then type "mongorestore". The sample DB should be loaded to your local mongo instance.

Steps to restore:

	git clone https://chefff@bitbucket.org/chefff/mongodump.git
	cd mongodump
	mongorestore  dump/
	



